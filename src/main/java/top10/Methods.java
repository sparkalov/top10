package top10;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 3 disfferent methods to calculate 10 maximum values.
 * 
 * @autor Viacheslav P.
 * 
 */
public class Methods {

  /**
   * Choose maximum 10 values from Stream by sorting of the all elements.
   * 
   * @param values stream of values.
   * @return list of maximum values.
   */
  public List<Integer> top10Sort(Stream<Integer> values) {
    return values.filter(Objects::nonNull).sorted(Comparator.reverseOrder()).limit(10)
        .collect(Collectors.toList());
  }

  /**
   * Choose maximum 10 values from Stream by sorting List with 10 elements faster than @see
   * {@link Methods#top10Sort}.
   * 
   * @param values stream of values.
   * @return list of maximum values.
   */
  public List<Integer> top10ForWithSort(Stream<Integer> values) {
    List<Integer> result = new ArrayList<>(10);
    values.filter(Objects::nonNull).forEach(item -> {
      /*
       * Add first 10 elements in result array.
       */
      if (result.size() < 10) {
        result.add(item);
        result.sort(Comparator.naturalOrder());
        /*
         * Check min element of result array.
         */
      } else if (result.get(0).compareTo(item) <= 0) {
        result.set(0, item);
        result.sort(Comparator.naturalOrder());
      }
    });
    return result.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
  }

  /**
   * Choose maximum 10 values from Stream by collecting in ArrayList faster than @see
   * {@link Methods#top10ForWithSort}
   *
   * 
   * @param values stream of values.
   * @return list of maximum values.
   */
  public List<Integer> top10For(Stream<Integer> values) {
    List<Integer> result = new ArrayList<>(10);
    values.filter(Objects::nonNull).forEach(item -> {
      /*
       * Add first element in result array.
       */
      if (result.size() == 0) {
        result.add(item);
      } else {
        for (int i = 0; i < result.size(); i++) {
          /*
           * Check current element of result array.
           */
          if (result.get(i).compareTo(item) <= 0) {
            /*
             * Check next element of result array if exist.
             */
            if ((result.size() > i + 1) && Objects.nonNull(result.get(i + 1))
                && result.get(i + 1).compareTo(item) <= 0) {
              continue;
            }
            /*
             * if array is full reorder elements from min to max.
             */
            if (result.size() == 10) {
              Integer replace = item;
              Integer current;
              for (int j = i; j >= 0; j--) {
                current = result.get(j);
                result.set(j, replace);
                replace = current;
              }
            } else {
              result.add(item);
            }
            break;
          }
          /*
           * Add items and sort from min to max until array is not full.
           */
          if (result.size() < 10) {
            result.add(item);
            result.sort(Comparator.naturalOrder());
            break;
          }
        }
      }
    });
    return result.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
  }
}
