package top10;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * Class with test for @see {@link Methods}
 * 
 * @autor Viacheslav P.
 *
 */
@RunWith(JUnit4.class)
public class MethodsTest {

  /**
   * Choose maximum 10 value from Stream of 100000000 values sorted by ascending using
   * {@link Methods#top10Sort}.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10MillionsAscBySort() {
    Methods methods = new Methods();
    List<Integer> top10 = new ArrayList<>(10);
    for (int i = 0; i < 10; i++) {
      top10.add(i, 99999999 - i);
    }
    List<Integer> result =
        methods.top10Sort(Stream.iterate(0, value -> value + 1).limit(100000000));
    assertNotNull(result);
    assertArrayEquals(top10.stream().toArray(Integer[]::new),
        result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of 100000000 values sorted by ascending using
   * {@link Methods#top10ForWithSort}.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10MillionsAscUsingSortWithFor() {
    Methods methods = new Methods();
    List<Integer> top10 = new ArrayList<>(10);
    for (int i = 0; i < 10; i++) {
      top10.add(i, 99999999 - i);
    }
    List<Integer> result =
        methods.top10ForWithSort(Stream.iterate(0, value -> value + 1).limit(100000000));
    assertNotNull(result);
    assertArrayEquals(top10.stream().toArray(Integer[]::new),
        result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of 100000000 values sorted by ascending using
   * {@link Methods#top10For}.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10MillionsAsc() {
    Methods methods = new Methods();
    List<Integer> top10 = new ArrayList<>(10);
    for (int i = 0; i < 10; i++) {
      top10.add(i, 99999999 - i);
    }
    List<Integer> result = methods.top10For(Stream.iterate(0, value -> value + 1).limit(100000000));
    assertNotNull(result);
    assertArrayEquals(top10.stream().toArray(Integer[]::new),
        result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of 100000000 values sorted by descending using
   * {@link Methods#top10ForWithSort}.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10MillionsDescUsingSortWithFor() {
    Methods methods = new Methods();
    List<Integer> top10 = new ArrayList<>(10);
    for (int i = 0; i < 10; i++) {
      top10.add(i, 100000000 - i);
    }
    List<Integer> result =
        methods.top10ForWithSort(Stream.iterate(100000000, value -> value - 1).limit(100000000));
    assertNotNull(result);
    assertArrayEquals(top10.stream().toArray(Integer[]::new),
        result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of 100000000 values sorted by descending using
   * {@link Methods#top10For}.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10MillionsDesc() {
    Methods methods = new Methods();
    List<Integer> top10 = new ArrayList<>(10);
    for (int i = 0; i < 10; i++) {
      top10.add(i, 100000000 - i);
    }
    List<Integer> result =
        methods.top10For(Stream.iterate(100000000, value -> value - 1).limit(100000000));
    assertNotNull(result);
    assertArrayEquals(top10.stream().toArray(Integer[]::new),
        result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of negative values.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10Negative() {
    Methods methods = new Methods();
    List<Integer> top10 = new ArrayList<>(10);
    for (int i = 0; i < 10; i++) {
      top10.add(i, 0 - i);
    }
    List<Integer> result = methods.top10For(Stream.iterate(0, value -> value - 1).limit(50));
    assertNotNull(result);
    assertArrayEquals(top10.stream().toArray(Integer[]::new),
        result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of random and null values.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10Random() {
    Methods methods = new Methods();
    Integer[] array = {null, 5, 18, null, 78, 50, 90, 3, 2, 5, 17, 109, 13, -1, 0};
    Integer[] checkArray = {109, 90, 78, 50, 18, 17, 13, 5, 5, 3};
    List<Integer> result = methods.top10For(Stream.of(array));
    assertNotNull(result);
    assertArrayEquals(checkArray, result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of null values.
   * 
   * @result Empty array without any errors.
   * 
   */
  @Test
  public void testTop10AllNulls() {
    Methods methods = new Methods();
    Integer[] array = {null, null, null, null, null, null, null, null};
    Integer[] checkArray = {};
    List<Integer> result = methods.top10For(Stream.of(array));
    assertNotNull(result);
    assertArrayEquals(checkArray, result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of random 5 values.
   * 
   * @result Values will be properly choose without any errors and array will not be
   *         <code>null</code>
   * 
   */
  @Test
  public void testTop10LessThan10() {
    Methods methods = new Methods();
    Integer[] array = {1, 2, 6, 0, -5};
    Integer[] checkArray = {6, 2, 1, 0, -5};
    List<Integer> result = methods.top10For(Stream.of(array));
    assertNotNull(result);
    assertArrayEquals(checkArray, result.stream().toArray(Integer[]::new));
  }

  /**
   * Choose maximum 10 value from Stream of 5 equal values.
   * 
   * @result Array of 5 equal values.
   * 
   */
  @Test
  public void testTop10All1() {
    Methods methods = new Methods();
    Integer[] array = {1, 1, 1, 1, 1};
    Integer[] checkArray = {1, 1, 1, 1, 1};
    List<Integer> result = methods.top10For(Stream.of(array));
    assertNotNull(result);
    assertArrayEquals(checkArray, result.stream().toArray(Integer[]::new));
  }
}
